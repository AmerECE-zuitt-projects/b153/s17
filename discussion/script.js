// values inside an array are called "elements"

let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades.indexOf(90.1));
// if indexOf cannot find your specified element inside of an array it returns to -1.
/* 
    if (emails.indexOf("example@mai.com") === -1) {
        user can register, because example@mail.com is not in the emails array.
    }
*/
grades[grades.length] = 85;
console.log(grades);

console.log(grades.indexOf(75));
let computerBrands = [
    "Acer",
    "Asus",
    "Lenovo",
    "Neo",
    "Redfox",
    "Gateway",
    "Toshiba",
    "Fujitsu"
];
console.log(computerBrands.length);

/* Arrays Methods
    .push() - adds an element at the end of an array and returns the array's length

    .pop() - remove an array's last element and returns the removed element.

    unshift() - add an element to the beginning of an array and returns the new length of the array

    shift() - remove the first element in the array.

    splice() - can add and remove from anywhere in an array, and even do both at once.
    .splice(starting index number, number of items to remove, items to add).
*/
grades.push(88);
console.log(grades);

// grades.pop();
// console.log(grades);
console.log(grades.pop());

grades.unshift(79);
console.log(grades);
grades.shift();
console.log(grades);

let tasks = [
    "shower",
    "eat breakfast",
    "go to work",
    "go home",
    "go to sleep"
];

// Add with splice
tasks.splice(3, 0, "eat lunch");
// console.log(tasks);

// Remove with splice
tasks.splice(2, 1);
// console.log(tasks);

// Add and remove with splice
tasks.splice(3, 1, "Code");
console.log(tasks);

// .sort() - rearranges array elements in alphanumeric order.
computerBrands.sort();
console.log(computerBrands);

// .reverse() - reverse the order of your array elements.
computerBrands.reverse();
console.log(computerBrands);

let a = [1, 2, 3, 4, 5];
let b= [6, 7, 8, 9, 10];
// .concat() - to combine two arrays the second array named will be added to the end of the first one.
let ab = a.concat(b);
console.log(ab);

// .join() - converts the elements of an array into a new string.
let joinedArray = ab.join(" and ");
console.log(joinedArray);

// Compare elements between arrays
let arr1 = [1, 2, 3];
let arr2 = [1, 2, 3];
console.log(arr1.join() === arr2.join());

// .slice() - copies specified ranged of array elements into new array
// .slice(starting point, stopping point), it will not copy the stopping point.
let c = a.slice(0, 3);
console.log(c);

// Mini-Activity
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let evenNumbers = [];

for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] % 2 === 0) {
        evenNumbers.push(numbers[i]);
    };
};
console.log(evenNumbers);

// Array iteration methods
// JavaScript loops only for arrays
// .forEach()

computerBrands.forEach(function (brand) {
    console.log(brand);
});

// .map() - create a new array populated with the results of running a function on each element of the mapped array.
let numbersArr = [1, 2, 3, 4, 5];
let doubledNumbers = numbersArr.map(function (number) {
    return number * 2;
});
console.log(doubledNumbers);

// .every() - check every element in an array and see if the all matches your given condition.
let allValid = numbersArr.every(function (number) {
    return number < 3;
});
console.log(allValid);

// .some() - check every element in an array and see if some match your given condition.
let someValid = numbersArr.some(function (number) {
    return number < 3;
});
console.log(someValid);

// .filter() - creates a new array only populated with elements that match our given condition.
let words = ["spray", "limit", "elite", "exuberant", "destruction", "present"];
let wordsResult = words.filter(function (word) {
    return word.length > 6;
});
console.log(wordsResult);

// .includes() - checks if the specified value is included in the array.
console.log(words.includes("limit"));

// 
let twoDimensional = [
    [1.1, 1.2, 1.3],
    [2.1, 2.2, 2.3],
    [3.1, 3.2, 3.3]
];
console.log(twoDimensional[0][1]);