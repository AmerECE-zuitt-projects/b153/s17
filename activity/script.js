// ---------------- Add Student ----------------
let studentList = [];
function addStudent (studentName) {
    studentList.push(studentName);
    console.log(`${studentName} was added to the student's list.`)
};
addStudent("John");
addStudent("Jane");
addStudent("Joe");

// ---------------- Count Students ----------------
function countStudents () {
    console.log(`There are total of ${studentList.length} students enrolled`);
};
countStudents();

// ---------------- Print Students ----------------
function printStudents () {
    studentList.sort();
    studentList.forEach((student) => {
        console.log(student);
    });
};
printStudents();

// ---------------- Find Student ----------------
function findStudent (studentName) {
    studentName = studentName.toLowerCase();
    let matches = studentList.filter((student) => {
        return student.toLowerCase().includes(studentName)
    });
    if (matches.length === 1) {
        console.log(`${matches} is an Enrollee`);
    }else if (matches.length > 1) {
        matches = matches.join(", ")
        console.log(`${matches} are Enrollees`);
    }else {
        console.log(`No student found with name ${studentName}`);
    };
};
findStudent("joe");
findStudent("bill");
findStudent("j");

// ---------------- Add Section ----------------
function addSection (section) {
    let newSection = studentList.map(student => student + ` - section ${section}`);
    console.log(newSection);
};
addSection("A")

// ---------------- Remove Student ----------------
function removeStudent (studentName) {
    studentName = studentName.toLowerCase();
    let lowerCaseStudentList = studentList.map((student) => {
        return student.toLowerCase();
    });
    for (let i = 0; i < lowerCaseStudentList.length; i++) {
        if (lowerCaseStudentList[i] === studentName) {
            lowerCaseStudentList.splice(i, 1);
            console.log(`${studentName} was removed from the student's list.`);
            console.log(lowerCaseStudentList);
        };
    };
};
removeStudent("joe");

// printStudents();

